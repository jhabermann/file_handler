import os,sys
from shutil import copyfile
sys.path.append(os.path.join(os.path.dirname(__file__), "lib"))

src = "C:/Users/jurij/.PyCharmCE2018.3/config/scratches/StartUp/env/src/"
dst = "C:/Users/jurij/.PyCharmCE2018.3/config/scratches/StartUp/env/dst/"
bup = "C:/Users/jurij/.PyCharmCE2018.3/config/scratches/StartUp/env/bup/"

def file_transport_handling():
    print("######## File - Transfer #########")
    for filename in os.listdir(src):
        print("Transfer File exists in source: " + str(filename))
        if filename.endswith('.txt'):
            print("Start transfer to destination: " + str(filename))
            copyfile(src + filename, dst + filename)
            print("File - Tranfer: " + str(filename) + " source to destination completed")
            print("Start transfer to backup: " + str(filename))
            copyfile(src + filename, bup + filename)
            print("File - Tranfer: " + str(filename) + " source to backup completed")

            if dst + filename:
                copyfile(src + filename, bup+ filename)
                print("Filename: " + str(filename) + " exists in backup")
                os.remove(src + filename)
                print("File: " + str(filename) + " deleted in source")

    print("Information: There are no files available for processing")
def delete_file_dest():
    for filename in os.listdir(dst):
        os.remove(dst + filename)
        print("File: " + str(filename) + " deleted in destination after FTP - Transfer")
#file_transport_handling()